import { Banner, Footer, Header, Slider } from './ArielBarrios'

function App() {

  return (
    <>
      <Header />
      <main>
        <Slider />
        <Banner />
      </main>
      <Footer />
    </>
  )
}

export default App
